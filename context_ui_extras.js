/*
 * @file
 * Make Block Reactions draggable.
 * 
 */

(function($) {
  // Create draggable blocks with droppable regions
  $(document).ready(function() {
    
    // Make block reactions draggable.
    $('.context-blockform-selector .form-type-checkbox').draggable({
      containment: 'table#context-blockform',
      start: function( event, ui ) {
        $('input[type=checkbox]', $(this)).attr('checked' , true);
  
      },
      stop: function(event, ui) {
      },
      revert: function(dropObj) {
        if(dropObj === false) {
         $('input[type=checkbox]', $(this)).attr('checked' , false);
         return true;
       }
       else
       {
         $('a.add-block', $(dropObj)).trigger('click');
         return false;
       }
    }
    });
   // Make the region headers droppable.
   $('#context-blockform td.blocks .label').droppable({
     hoverClass: "context_block_reaction_hover"
   });
  
  });
})(jQuery);